package pgl;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class PglTest {

	@Test
	public void testImputString() {
		String inputString ="Hello Word";
		Plg transletor=new Plg(inputString);
		assertEquals("Hello Word",transletor.getString());
	}
	
		@Test
		public void testTradStringaNulla() {
			String inputString ="";
			Plg transletor=new Plg(inputString);
			assertEquals("nil",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConAFinisceConY() {
			String inputString ="any";
			Plg transletor=new Plg(inputString);
			assertEquals("anynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConOFinisceConY() {
			String inputString ="only";
			Plg transletor=new Plg(inputString);
			assertEquals("onlynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConIFinisceConY() {
			String inputString ="imperturbability";
			Plg transletor=new Plg(inputString);
			assertEquals("imperturbabilitynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConEFinisceConY() {
			String inputString ="empty";
			Plg transletor=new Plg(inputString);
			assertEquals("emptynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConUFinisceConY() {
			String inputString ="ugly";
			Plg transletor=new Plg(inputString);
			assertEquals("uglynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConVocaleFinisceConY() {
			String inputString ="ability";
			Plg transletor=new Plg(inputString);
			assertEquals("abilitynay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConVocaleFinisceConVocale() {
			String inputString ="apple";
			Plg transletor=new Plg(inputString);
			assertEquals("appleyay",transletor.getTranslete());
	}

		@Test
		public void testTradFraseIniziaConUFinisceConE() {
			String inputString ="unacceptable";
			Plg transletor=new Plg(inputString);
			assertEquals("unacceptableyay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConVocaleFinisceConConsonate() {
			String inputString ="ask";
			Plg transletor=new Plg(inputString);
			assertEquals("askay",transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConSingolaConsonante() {
			String inputString ="hello"  ;
			Plg transletor=new Plg(inputString);
			assertEquals("ellohay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConSingolaConsonante2() {
			String inputString ="cat"  ;
			Plg transletor=new Plg(inputString);
			assertEquals("atcay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradFraseIniziaConPiuConsonanti() {
			String inputString = "known";
			Plg transletor=new Plg(inputString);
			assertEquals("ownknay" ,transletor.getTranslete());
	}
		@Test
		public void testTradFraseIniziaConPiuConsonanti2() {
			String inputString = "start";
			Plg transletor=new Plg(inputString);
			assertEquals("artstay" ,transletor.getTranslete());
	}

		@Test
		public void testTradDiPiuFrasiSeparateDaSpazio() {
			String inputString = "hello world";
			Plg transletor=new Plg(inputString);
			assertEquals("ellohay orldway" ,transletor.getTranslete());
	}
		@Test
		public void testTradDiPiuFrasiSeparateDaSpazio2() {
			String inputString = "hello everybody welcome to university";
			Plg transletor=new Plg(inputString);
			assertEquals("ellohay everybodynay elcomeway otay universitynay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradDiPiuFrasiSeparateDaTrattino() {
			String inputString = "well-being";
			Plg transletor=new Plg(inputString);
			assertEquals("ellway-eingbay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradDiPiuFrasiSeparateDaTrattino2() {
			String inputString = "the amazing spider-man";
			Plg transletor=new Plg(inputString);
			assertEquals("ethay amazingay iderspay-anmay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradfrasiContenentiPunteggiatura() {
			String inputString = "hello world!";
			Plg transletor=new Plg(inputString);
			assertEquals("ellohay orldway!" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradfrasiContenentiPunteggiatura2() {
			String inputString = "('happy new year!!!')";
			Plg transletor=new Plg(inputString);
			assertEquals("('appyhay ewnay earyay!!!')" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradfrasiContenentiPunteggiatura3() {
			String inputString = "('happy-new year!!!')";
			Plg transletor=new Plg(inputString);
			assertEquals("('appyhay-ewnay earyay!!!')" ,transletor.getTranslete());
	}
		@Ignore
		@Test
		public void testTradfrasiContenentiPunteggiatura4() {
			String inputString = "('hi!-happy-new year!!!')";
			Plg transletor=new Plg(inputString);
			assertEquals("('ihay!-appyhay-ewnay earyay!!!')" ,transletor.getTranslete());
	}
		@Test
		public void testTradfrasiMaiuscole() {
			String inputString = "APPLE";
			Plg transletor=new Plg(inputString);
			assertEquals("APPLEYAY" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradfrasiTitleCase() {
			String inputString = "Hello";
			Plg transletor=new Plg(inputString);
			assertEquals("Ellohay" ,transletor.getTranslete());
	}
		
		@Test
		public void testTradfrasiCheSollevanoUnEccezzione() {
			String inputString = "hello{world";
			Plg transletor=new Plg(inputString);
			assertEquals("this phrasing cannot be translated" ,transletor.getTranslete());
	}
		
}

package pgl;

public class PigLatinException extends RuntimeException {
	public PigLatinException(String message) {
		super(message);
	}

}

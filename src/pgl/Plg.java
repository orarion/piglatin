package pgl;
import java.util.ArrayList;
public class Plg {
	private static String frase;
	public static final String Nil ="nil";
	public Plg(String inputString) {
		frase= inputString;
	}

	public static String getString() {

		return frase;
	}

	public String getTranslete() {
		if(frase.equals("")) {
			return Nil;
		}
		if(!ContolloDiStringaSoloAlfabetica(frase)) {
			ArrayList<String> stringa=new ArrayList<>();
			char carattere='0';
			try {
				carattere=individuaSpaziatura_E_Punteggiatura(frase);
				dividiStringa(0,(frase.indexOf(carattere)),stringa,frase,carattere);
			}
			catch(PigLatinException pglE){
				return "this phrasing cannot be translated";
			}
			if(frase.matches("[A-Z]*")) {
				traduciPiuParole(stringa,frase).toUpperCase();
			}else {
				return traduciPiuParole(stringa,frase);
			}
			

		}else {
			if(frase.matches("[A-Z]*")) {
				return traduciSingolaParola(frase).toUpperCase();
			}else if(frase.matches("[a-z]*")) {
				return traduciSingolaParola(frase);
			}else if(frase.substring(0,1).matches("[A-Z]*")&&frase.substring(1).matches("[a-z]*")) {
				String parola=traduciSingolaParola(frase).toLowerCase();
				return parola.substring(0,1).toUpperCase()+parola.substring(1);
			}
		}
		return Nil;		
	}

	private boolean iniziaPerVocale(String parola) {
		return (parola.startsWith("a")||parola.startsWith("A")||parola.startsWith("e")||parola.startsWith("E")||parola.startsWith("i")||parola.startsWith("I")||parola.startsWith("o")||parola.startsWith("O")||parola.startsWith("u")||parola.startsWith("U")) ;
	}

	private boolean finiscePerVocale(String parola) {
		return (parola.endsWith("a")||parola.endsWith("A")||parola.endsWith("e")||parola.endsWith("E")||parola.endsWith("i")||parola.endsWith("I")||parola.endsWith("o")||parola.endsWith("O")||parola.endsWith("u")||parola.endsWith("U")) ;
	}

	private String iniziaPerConsonante(String parola) {
		String lettere="";
		int i;
		for (i=0;i<parola.length();i++) {
			if(("aeiou".indexOf(parola.charAt(i)))!=-1||("AEIOU".indexOf(parola.charAt(i)))!=-1) {
				break;
			}else {
				char lettera=parola.charAt(i);
				lettere+=lettera;
			}
		}
		return parola.substring(i)+lettere+"ay";
	}

	private String traduciSingolaParola(String parola) {
		if(iniziaPerVocale(parola)) {
			if(parola.endsWith("y")) {
				return parola+"nay";
			} else if (finiscePerVocale(parola)) {
				return parola+"yay";
			}else if(!finiscePerVocale(parola)) {
				return parola+"ay";
			}
		}else if(!iniziaPerVocale(parola)&&!parola.equals("")) {
			return iniziaPerConsonante(parola);
		}
		return Nil;
	}

	private void dividiStringa(int i,int j, ArrayList<String> stringa,String phrase,char carattere) throws PigLatinException{
		String dividi=phrase.substring(i,j);
		if(ContolloDiStringaSoloAlfabetica(dividi)) {
			stringa.add(dividi);
			carattere=individuaSpaziatura_E_Punteggiatura(phrase.substring(j+1));
			if(phrase.substring(j+1).indexOf(carattere)!=-1) {
				dividiStringa(j+1,(phrase.substring(j+1).indexOf(carattere)+j+1),stringa,phrase,carattere); 
			}else { 
				stringa.add(phrase.substring(j+1));
			}
		}else {
			carattere=individuaSpaziatura_E_Punteggiatura(dividi);
			if(phrase.indexOf(carattere)==0) {
				phrase=phrase.substring(1,phrase.length());
				dividiStringa(0,j,stringa,phrase,carattere);
			}else {
				if(stringa.isEmpty()) {
					dividiStringa(0,phrase.indexOf(carattere),stringa,phrase,carattere);
				}else {
					dividiStringa(i,phrase.indexOf(carattere),stringa,phrase,carattere);
				}
			}
		}
	}
	private String traduciPiuParole(ArrayList<String>string,String phrase) {
		String parola="";
		String st;
		int s=0,t=0;
		if(ContolloDiStringaSoloAlfabetica(phrase.substring(0, 1))) {
			parola=traduciSingolaParola(string.get(0));
			t=(string.get(s).length());
			st=string.get(0);
			s++;
		}else {
			parola=phrase.substring(0,1);
			t=1;
			st=phrase.substring(0,1);
		}
		for(int i=t;i<phrase.length()&&!phrase.equals(st);i++) {
			if((i+1)<=phrase.length()&&s<string.size()) {
				if(ContolloDiStringaSoloAlfabetica(phrase.substring(i, i+1))) {
					parola+=traduciSingolaParola(string.get(s));
					i+=(string.get(s).length());
					st+=string.get(s);
					s++;
					i-=1;
				}else {
					parola+=phrase.charAt(i);
					st+=phrase.charAt(i);
				}
			}		
		}
		return parola;
	}


	private char individuaSpaziatura_E_Punteggiatura (String parola) throws PigLatinException {
		if (parola.indexOf(' ')!=-1) {
			return' ';
		}else if (parola.indexOf('-')!=-1){
			return'-';
		}else if (parola.indexOf('.')!=-1) {
			return'.';
		}else if(parola.indexOf(':')!=-1) {
			return':';
		}else if(parola.indexOf(',')!=-1) {
			return',';
		}else if(parola.indexOf(';')!=-1) {
			return';';
		}else if(parola.indexOf('!')!=-1) {
			return'!';
		}else if(parola.indexOf('?')!=-1) {
			return'?';
		}else if(parola.indexOf('(')!=-1) {
			return'(';
		}else if(parola.indexOf(')')!=-1) {
			return')';
		}else if(parola.indexOf('\'')!=-1) {
			return'\'';
		}else if(!(ContolloDiStringaSoloAlfabetica(parola))){

			throw new PigLatinException("character not accepted ");
		}
		return 0;
	}

	private boolean ContolloDiStringaSoloAlfabetica(String parola) {
		return(parola.matches("[a-zA-Z]*"));
	}

}
